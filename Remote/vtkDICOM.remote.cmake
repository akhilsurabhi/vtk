#
# Dicom Classes
#

vtk_fetch_module(vtkDICOM
  "Dicom classes and utilities"
  GIT_REPOSITORY https://github.com/dgobbi/vtk-dicom
  # vtk-dicom tag v0.7.0 plus 2 (Oct 23, 2015)
  GIT_TAG e32ec575b084735b90cfacff589db35ce4cfc017
  )
